
SELECT @max_limbs := MAX(arms+legs) FROM limbs;
SELECT * FROM limbs WHERE arms+legs = @max_limbs;

SELECT @last_id := LAST_INSERT_ID();

SELECT @name := thing FROM limbs WHERE legs = 0;

SELECT @name2 := thing FROM limbs WHERE legs < 0;

SET @sum = 4 + 7;
SELECT @sum;

SET @max_limbs = (SELECT MAX(arms+legs) FROM limbs);

SET @x=1, @X=2;
SELECT @x, @X;
