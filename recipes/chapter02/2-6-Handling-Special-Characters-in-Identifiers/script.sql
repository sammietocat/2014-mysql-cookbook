
DROP TABLE IF EXISTS `some table`;
CREATE TABLE `some table` (i INT);

# must use with ANSI_QUOTES mode enabled
# DROP TABLE IF EXISTS "some table";
# CREATE TABLE "some table" (i INT);

SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='cookbook' AND TABLE_NAME='profile';
