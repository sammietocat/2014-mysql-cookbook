package main

import (
	"database/sql"

	"github.com/go-sql-driver/mysql"
)

func main() {
	// specify configuration for db
	config := mysql.Config{
		User:   "sammy",
		Passwd: "hello",
		Net:    "tcp",
		Addr:   "localhost:3306",
		DBName: "cookbook",
	}

	// Open db connection
	db, err := sql.Open("mysql", config.FormatDSN())
	if nil != err {
		panic(err.Error())
	}
	defer db.Close()

	// Validate DSN data
	err = db.Ping()
	if nil != err {
		panic(err.Error())
	}
}
