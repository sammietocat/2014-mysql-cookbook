package main

import (
	"database/sql"
	"fmt"

	"github.com/go-sql-driver/mysql"
)

func logMySQLError(err error) {
	mErr, ok := err.(*mysql.MySQLError)
	if ok {
		fmt.Printf("%+v\n", *mErr)
	} else {
		fmt.Println("It's not a *mysql*MySQLError")
	}
}

func connect() (*sql.DB, error) {
	// specify configuration for db
	config := mysql.Config{
		User:   "sammy1",
		Passwd: "hello",
		Net:    "tcp",
		Addr:   "localhost:3306",
		DBName: "cookbook",
	}

	// Open db connection
	db, err := sql.Open("mysql", config.FormatDSN())
	if nil != err {
		return nil, err
	}
	//defer db.Close()

	// Validate DSN data
	err = db.Ping()
	if nil != err {
		return nil, err
	}

	return db, nil
}

func main() {
	db, err := connect()
	if nil != err {
		logMySQLError(err)
		panic(err.Error())
	}
	db.Close()
}
