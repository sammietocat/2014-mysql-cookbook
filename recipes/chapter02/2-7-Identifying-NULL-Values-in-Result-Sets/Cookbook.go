package main

import (
	"database/sql"
	"fmt"

	"github.com/go-sql-driver/mysql"
)

func connect() (*sql.DB, error) {
	// specify configuration for db
	config := mysql.Config{
		User:   "sammy",
		Passwd: "hello",
		Net:    "tcp",
		Addr:   "localhost:3306",
		DBName: "cookbook",
	}

	// Open db connection
	db, err := sql.Open("mysql", config.FormatDSN())
	if nil != err {
		return nil, err
	}
	//defer db.Close()

	// Validate DSN data
	err = db.Ping()
	if nil != err {
		return nil, err
	}

	return db, nil
}

func main() {
	db, err := connect()
	if nil != err {
		panic(err.Error())
	}
	defer db.Close()

	stmtSel, err := db.Prepare("SELECT name, birth, foods FROM profile")
	if nil != err {
		panic(err.Error())
	}
	defer stmtSel.Close()

	rows, err := stmtSel.Query()
	if nil != err {
		panic(err.Error())
	}
	defer rows.Close()

	var name sql.NullString
	var birth mysql.NullTime
	var foods sql.NullString
	for rows.Next() {
		err = rows.Scan(&name, &birth, &foods)
		if nil != err {
			panic(err.Error())
		}

		if name.Valid {
			fmt.Print(name.String)
		} else {
			fmt.Print("NULL")
		}

		if birth.Valid {
			fmt.Print(",", birth.Time)
		} else {
			fmt.Print(",NULL")
		}

		if foods.Valid {
			fmt.Println(",", foods.String)
		} else {
			fmt.Println(",NULL")
		}
	}
}
