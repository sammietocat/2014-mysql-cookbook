package main

import (
	"database/sql"
	"fmt"

	"github.com/go-sql-driver/mysql"
)

func connect() (*sql.DB, error) {
	// specify configuration for db
	config := mysql.Config{
		User:   "sammy",
		Passwd: "hello",
		Net:    "tcp",
		Addr:   "localhost:3306",
		DBName: "cookbook",
	}

	// Open db connection
	db, err := sql.Open("mysql", config.FormatDSN())
	if nil != err {
		return nil, err
	}
	//defer db.Close()

	// Validate DSN data
	err = db.Ping()
	if nil != err {
		return nil, err
	}

	return db, nil
}

func main() {
	db, err := connect()
	if nil != err {
		panic(err.Error())
	}
	defer db.Close()

	// statements return no result set
	res, err := db.Exec("UPDATE profile SET cats = cats+1 WHERE name = 'Sybil'")
	if nil != err {
		panic(err.Error())
	}

	n, err := res.RowsAffected()
	if nil == err {
		fmt.Println("rows affected:", n)
	}

	// statements return a result set
	rows, err := db.Query("SELECT id, name, cats FROM profile")
	if nil != err {
		panic(err.Error())
	}
	defer rows.Close()

	// display columns name
	cols, err := rows.Columns()
	if nil != err {
		panic(err.Error())
	}
	fmt.Println(cols)

	var id int
	var name string
	var cats int
	for rows.Next() {
		err = rows.Scan(&id, &name, &cats)
		fmt.Println(id, name, cats)
	}
}
