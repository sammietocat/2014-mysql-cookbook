
SELECT * FROM profile WHERE color='green'; 

INSERT INTO profile (name,color) VALUES('Gary', 'blue');

INSERT INTO profile (name,birth,color,foods,cats) VALUES('Alison','1973-01-12','blue','eggroll',4);

INSERT INTO profile (name,birth,color,foods,cats) VALUES('De''Mont','1973-01-12','blue','eggroll',4);
# INSERT INTO profile (name,birth,color,foods,cats) VALUES('De\'Mont','1973-01-12','blue','eggroll',4);
INSERT INTO profile (name,birth,color,foods,cats) VALUES("De\'Mont",'1973-01-12','blue','eggroll',4);

# error one
# INSERT INTO profile (name,birth,color,foods,cats) VALUES("De''Mont",'1973-01-12','NULL','eggroll',4);
# ok one
INSERT INTO profile (name,birth,color,foods,cats) VALUES("De''Mont",'1973-01-12',NULL,'eggroll',4);

