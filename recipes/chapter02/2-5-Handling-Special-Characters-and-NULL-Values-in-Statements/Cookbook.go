package main

import (
	"database/sql"
	"fmt"

	"github.com/go-sql-driver/mysql"
)

func connect() (*sql.DB, error) {
	// specify configuration for db
	config := mysql.Config{
		User:   "sammy",
		Passwd: "hello",
		Net:    "tcp",
		Addr:   "localhost:3306",
		DBName: "cookbook",
	}

	// Open db connection
	db, err := sql.Open("mysql", config.FormatDSN())
	if nil != err {
		return nil, err
	}
	//defer db.Close()

	// Validate DSN data
	err = db.Ping()
	if nil != err {
		return nil, err
	}

	return db, nil
}

func main() {
	db, err := connect()
	if nil != err {
		panic(err.Error())
	}
	defer db.Close()

	stmtIn, err := db.Prepare("INSERT INTO profile (name,birth,color,foods,cats) VALUES(?,?,?,?,?)")
	if nil != err {
		panic(err.Error())
	}
	defer stmtIn.Close()

	res, err := stmtIn.Exec("De'Mont", "1973-01-12", nil, "eggroll", 4)
	if nil != err {
		panic(err.Error())
	}

	id, err := res.LastInsertId()
	if nil != err {
		panic(err.Error())
	}
	fmt.Println("Id of last insert profile is", id)
}
