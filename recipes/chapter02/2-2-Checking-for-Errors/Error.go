package main

import (
	"database/sql"
	"fmt"

	"github.com/go-sql-driver/mysql"
)

func logMySQLError(err error) {
	mErr, ok := err.(*mysql.MySQLError)
	if ok {
		fmt.Printf("%+v\n", *mErr)
	} else {
		fmt.Println("Conversion to *mysql.MySQLError failed")
	}
}

func main() {
	// specify configuration for db
	config := mysql.Config{
		User:   "baduser",
		Passwd: "badpass",
		Net:    "tcp",
		Addr:   "localhost:3306",
		DBName: "cookbook",
	}

	// Open db connection
	db, err := sql.Open("mysql", config.FormatDSN())
	if nil != err {
		logMySQLError(err)
		panic(err.Error())
	}
	defer db.Close()

	fmt.Println("after sql.Open()")

	// Validate DSN data
	err = db.Ping()
	if nil != err {
		logMySQLError(err)
		panic(err.Error())
	}
}
