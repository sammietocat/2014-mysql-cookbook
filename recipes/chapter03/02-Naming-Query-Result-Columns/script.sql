
SELECT t, srcuser, srchost, size FROM mail;

SELECT DATE_FORMAT(t,'%M %e, %Y'), CONCAT(srcuser,'@',srchost), size FROM mail;
SELECT DATE_FORMAT(t,'%M %e, %Y') AS date_sent, CONCAT(srcuser,'@',srchost) AS sender, size FROM mail;
SELECT DATE_FORMAT(t,'%M %e, %Y') AS 'Date of message', CONCAT(srcuser,'@',srchost) AS 'Message sender', size AS 'Number of bytes' FROM mail;

SELECT 1 AS 'INTEGER';
