
SELECT DATE_FORMAT(t,'%M %e, %Y') AS date_sent, CONCAT(srcuser,'@',srchost) AS sender, CONCAT(dstuser,'@',dsthost) AS recipent, size FROM mail;

#CREATE VIEW mail_view AS SELECT DATE_FORMAT(t,'%M %e, %Y') AS date_sent, CONCAT(srcuser,'@',srchost) AS sender, CONCAT(dstuser,'@',dsthost) AS recipent, size FROM mail;

SELECT date_sent, sender, size FROM mail_view WHERE size>100000 ORDER BY size;
