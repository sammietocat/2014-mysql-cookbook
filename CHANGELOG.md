# Tour of ``MySQL Cookbook, Third Edition by Paul DuBois (O'Reilly)''  

## [2017-09-09]  
+ **Added**  
  - Chapter 01 - Using the `mysql` Client Program  
	- Chapter 02 - Writing MySQL-Based Programs  
